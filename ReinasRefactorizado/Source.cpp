#include "Tablero.h"
#include "Timer.h"
#define TAM 9

pair<int, int> elegirReglaModificación(vector <pair<int, int>>* reglas)
{
	pair<int, int> elegido = *reglas->begin(); //Elegir de forma lineal
	reglas->erase(reglas->begin());
	return elegido;
}

Tablero aplicarReglaModificacion(pair<int, int> regla, Tablero estadoInicial)
{
	Tablero nuevo = estadoInicial;
	nuevo.posicionarReina(regla.second, regla.first);
	return nuevo;
}

bool cumpleCondicionesDeTerminacion(Tablero aRevisar)
{
	if (aRevisar.contarReinasPosicionadas() == TAM - 1)
		return true;
	else
		return false;
}

void inicializar(int estadoInicial[TAM])
{
	for (int i = 1; i < TAM; i++)
	{
		estadoInicial[i] = 0;
	}
}

Tablero estrategiaDeControlBFS(int estadoInicial[TAM], int *tablerosVisitados, timer* reloj)
{
	vector<Tablero> cola;
	bool encontre = false;
	reloj->restart();

	Tablero inicio(estadoInicial);
	Tablero solucion(estadoInicial);

	reloj->start();
	cola.push_back(inicio);
	while (cola.size() != 0 && !encontre)
	{
		vector<pair<int, int>> reglas = cola[0].devolverPosiblesReglasAplicación();
		while (reglas.size() != 0 && !encontre)
		{
			pair<int, int> eleccion = elegirReglaModificación(&reglas);
			Tablero nuevo = aplicarReglaModificacion(eleccion, cola[0]);
			if (cumpleCondicionesDeTerminacion(nuevo))
			{
				solucion = nuevo;
				reloj->stop();
				encontre = true;
			}
			cola.push_back(nuevo);
		}
		tablerosVisitados++;
		cola.erase(cola.begin());
	}
	return solucion;
}

void dibujarTablero(Tablero tablero)
{
	for (int i = 1; i < TAM; i++)
	{
		for (int j = 1; j < TAM; j++)
		{
			if (j == tablero.devolverColumnaReina(i))
				cout << "R\t";
			else
				cout << "_\t";
		}
		cout << endl;
	}
}
int main()
{
	int tablerosVisitados = 0;
	timer reloj;
	int estadoInicial[TAM];
	inicializar(estadoInicial);

	Tablero solucion = estrategiaDeControlBFS(estadoInicial, &tablerosVisitados, &reloj);
	
	solucion.mostrarEstadoActual();
	cout << "Tableros visitados: " << tablerosVisitados << endl;
	cout << "Tiempo (Microsegundos): " << reloj << endl;
	dibujarTablero(solucion);
	return 0;
}