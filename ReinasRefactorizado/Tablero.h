#pragma once
#include <iostream>
#include <vector>
using namespace std;
#define TAM 9
class Tablero
{
private:
	int reinas[TAM]; //Aqui se alojan las reinas;
public:
	Tablero(int estadoInicial[TAM]);
	~Tablero();
	void mostrarEstadoActual();
	int contarReinasPosicionadas();
	vector<pair<int, int>> devolverPosiblesReglasAplicación();
	void posicionarReina(int columna, int fila);
	int devolverColumnaReina(int fila);
};

