#include "Tablero.h"

Tablero::Tablero(int estadoInicial[TAM])
{
	for (int i = 1; i < TAM; i++)
	{
		reinas[i] = estadoInicial[i];
	}
}

Tablero::~Tablero()
{
}

void Tablero::mostrarEstadoActual()
{
	for (int i = 1; i < TAM - 1 ; i++)
	{
		cout << reinas[i] << ", ";
	}
	cout << reinas[TAM - 1] << endl;
}

int Tablero::contarReinasPosicionadas()
{
	int contadorReinas = 0;
	for (int i = 1; i < TAM; i++)
	{
		if(reinas[i] != 0)
			contadorReinas++;
	}
	return contadorReinas;
}

vector<pair<int, int>> Tablero::devolverPosiblesReglasAplicación()
{
	pair<int, int> auxiliar, reina;
	int columna;
	vector<pair<int, int>> reglasAplicables;
	int reinasColocadas = contarReinasPosicionadas();
	int filaAEvaluar = reinasColocadas + 1;
	auxiliar.first = filaAEvaluar;
	for(int i = 1; i<TAM;i++)
	{
		auxiliar.second = i;
		bool esAtacada = false;
		for (int j = 1; j < TAM; j++) //Recorre todo el vector de reinas
		{
			if (reinas[j] != 0)
			{
				reina.first = j;
				reina.second = reinas[j];
				if (reina.second == i) {
					esAtacada = true;
					j = TAM;
				}
				else
				{
					if (abs(reina.first - auxiliar.first) == abs(reina.second - auxiliar.second)) {
						esAtacada = true;
						j = TAM;
					}
					else
					{
						esAtacada = false;
					}
				}
				
			}
		}
		if (esAtacada == false)
			reglasAplicables.push_back(auxiliar);
	}
	return reglasAplicables;
}

void Tablero::posicionarReina(int columna, int fila)
{
	reinas[fila] = columna;
}

int Tablero::devolverColumnaReina(int fila)
{
	return reinas[fila];
}
